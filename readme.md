# Super Parser

Takes the file `input.md` and outputs it as a json file

## Example Input

---

````markdown
# Titre du document

Voici un paragraphe

## Titre de niveau 2

Un nouveau paragraphe
Toujours le même paragraphe

Et un nouveau paragraphe !

```shell
# Un bloc de code en shell
echo "coucou les lapinous"
```

### Un titre de niv 3

test
etstg

- une liste
  # azeqsdfd
  - # sdqd
    - azeaze
      - azeqsd
        - # sdsd
- à
- puce

```javascript
# Un bloc de code en shell
echo "coucou les lapinous"
```
````

## Example Output v1

---

```json
[
  { "type": "h1", "text": " Titre du document" },
  { "type": "p", "text": "Voici un paragraphe" },
  { "type": "h2", "text": " Titre de niveau 2" },
  { "type": "p", "text": "Un nouveau paragraphe\nToujours le même paragraphe\nEt un nouveau paragraphe !" },
  { "type": "code", "language": "shell", "content": ["# Un bloc de code en shell", "echo \"coucou les lapinous\""] },
  { "type": "h3", "text": " Un titre de niv 3" },
  { "type": "p", "text": "test\netstg" },
  { "type": "list", "content": [" une liste"] },
  { "type": "h1", "text": " azeqsdfd" },
  { "type": "list", "content": [" # sdqd", " azeaze", " azeqsd", " # sdsd", " à", " puce"] },
  {
    "type": "code",
    "language": "javascript",
    "content": ["# Un bloc de code en shell", "echo \"coucou les lapinous\""]
  }
]
```

## Example Output v2

---

```json
[
  {
    "type": "h1",
    "text": " Titre du document",
    "content": [
      { "type": "p", "text": "Voici un paragraphe" },
      {
        "type": "h2",
        "text": " Titre de niveau 2",
        "content": [
          { "type": "p", "text": "Un nouveau paragraphe\nToujours le même paragraphe\nEt un nouveau paragraphe !" },
          {
            "type": "code",
            "language": "shell",
            "content": ["# Un bloc de code en shell", "echo \"coucou les lapinous\""]
          },
          {
            "type": "h3",
            "text": " Un titre de niv 3",
            "content": [
              { "type": "p", "text": "test\netstg" },
              { "type": "list", "content": [" une liste"] }
            ]
          }
        ]
      }
    ]
  },
  {
    "type": "h1",
    "text": " azeqsdfd",
    "content": [
      { "type": "list", "content": [" # sdqd", " azeaze", " azeqsd", " # sdsd", " à", " puce"] },
      {
        "type": "code",
        "language": "javascript",
        "content": ["# Un bloc de code en shell", "echo \"coucou les lapinous\""]
      }
    ]
  }
]
```

_Mathieu GARNIER_  
_Zoïa KERCHOWIEC_  
_Cyirl DINTHEER_
