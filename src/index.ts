import fs from "fs";
import path from "path";

export enum ParserState {
  None,
  List,
  Code,
}

export enum ItemAction {
  None, // do nothing
  Push, // push new item to results
  Add, // add to last result
  AddOrPush, // check last result, if same type then add else push
}

export enum ItemType {
  None = "",
  H1 = "h1",
  H2 = "h2",
  H3 = "h3",
  List = "list",
  Code = "code",
  P = "p",
}

export const itemTypeHierarchies = new Map<string, number>([
  [ItemType.None, 99999],
  [ItemType.H1, 1],
  [ItemType.H2, 10],
  [ItemType.H3, 100],
  [ItemType.P, 10000],
  [ItemType.Code, 10000],
  [ItemType.List, 10000],
]);

export interface LineAction {
  newState: ParserState;
  itemAction: ItemAction;
  item: ResultItem;
}

export interface ResultItem {
  type: ItemType;
  text?: string;
  content?: string[];
  language?: string;
}

// https://codepen.io/kvendrik/pen/Gmefv

/**
 * from the path of a file, return all of its lines as a string array
 *
 * @param src
 * @returns
 */
export const getFileLines = (src: string): string[] =>
  fs.readFileSync(src).toString().replace(/\r\n/g, "\r").replace(/\n/g, "\r").split("\r");

/**
 * return the next action depending on the string passed in params
 * if the line starts with ```, it means that the next action is not a code action (code block is closed)
 * if not, return a new code action with the text set as the line
 *
 * @param line
 * @returns
 */
export const processCodeLine = (line: string): LineAction => {
  if (line.startsWith("```")) {
    return { newState: ParserState.None, itemAction: ItemAction.None, item: { type: ItemType.None } };
  } else {
    return { newState: ParserState.Code, itemAction: ItemAction.Add, item: { type: ItemType.None, text: line } };
  }
};

/**
 * return the next action depending on the string passed in params
 * if the line starts with -, it means that the next action is list action (list block is not yet closed)
 * if not, process the line as it is a normal one
 *
 * @param line
 * @returns
 */
export const processListLine = (line: string): LineAction => {
  if (line.startsWith("-")) {
    return {
      newState: ParserState.List,
      itemAction: ItemAction.Add,
      item: { type: ItemType.None, text: line.substring(1) },
    };
  } else {
    return processNormalLine(line);
  }
};

/**
 * return the next action depending on the string passed in params
 * if the line starts with ```, it means taht the next action is code action (code block just started)
 * if the line starts with -, it means that the next action is list action (list block just started)
 * if the line starts with ###, it means that the actual is H3
 * if the line starts with ##, it means that the actual is H2
 * if the line starts with #, it means that the actual is H1
 * if the line doesnt starts with anything special and is defined, it means the line is a p line
 * if the line is undefined, return the "none" action
 *
 * @param line
 * @returns
 */
export const processNormalLine = (line: string): LineAction => {
  if (line.startsWith("```")) {
    return {
      newState: ParserState.Code,
      itemAction: ItemAction.Push,
      item: { type: ItemType.Code, language: line.substring(3), content: [] },
    };
  } else if (line.startsWith("-")) {
    return {
      newState: ParserState.List,
      itemAction: ItemAction.Push,
      item: { type: ItemType.List, content: [line.substring(1)] },
    };
  } else if (line.startsWith("###")) {
    return {
      newState: ParserState.None,
      itemAction: ItemAction.Push,
      item: { type: ItemType.H3, text: line.substring(3) },
    };
  } else if (line.startsWith("##")) {
    return {
      newState: ParserState.None,
      itemAction: ItemAction.Push,
      item: { type: ItemType.H2, text: line.substring(2) },
    };
  } else if (line.startsWith("#")) {
    return {
      newState: ParserState.None,
      itemAction: ItemAction.Push,
      item: { type: ItemType.H1, text: line.substring(1) },
    };
  } else {
    if (line !== "") {
      return { newState: ParserState.None, itemAction: ItemAction.AddOrPush, item: { type: ItemType.P, text: line } };
    } else {
      return { newState: ParserState.None, itemAction: ItemAction.None, item: { type: ItemType.None } };
    }
  }
};

/**
 * recursive function to transform all markdownLines into js objects
 * it will act differently depending on the actual parserState
 * the parserState will be updated on each line
 * it will return a list of resultItem
 *
 * @param markdownLines
 * @param index
 * @param parserState
 * @param results
 * @returns
 */
export const parseMarkdown = (
  markdownLines: string[],
  index: number,
  parserState: ParserState,
  results: ResultItem[]
): ResultItem[] => {
  const line = markdownLines[index];
  if (line !== undefined) {
    const lineStartIndex = line.search(/\S|$/);
    const lineWithoutStartSpaces = line.substring(lineStartIndex);

    const lineAction = getLineAction(parserState, lineWithoutStartSpaces);

    return parseMarkdown(markdownLines, index + 1, lineAction.newState, processLineAction(lineAction, results));
  } else {
    return results;
  }
};

/**
 * get the lineAction for this state on this line
 *
 * @param state
 * @param lineWithoutStartSpaces
 * @returns
 */
export const getLineAction = (state: ParserState, lineWithoutStartSpaces: string): LineAction => {
  switch (state) {
    case ParserState.Code:
      return processCodeLine(lineWithoutStartSpaces);
    case ParserState.List:
      return processListLine(lineWithoutStartSpaces);
    case ParserState.None:
    default:
      return processNormalLine(lineWithoutStartSpaces);
  }
};

/**
 * take a lineAction and resultItems
 * return a new resultItem list with the lineAction processed in it
 *
 * @param lineAction
 * @param result
 * @returns
 */
export const processLineAction = (lineAction: LineAction, result: ResultItem[]): ResultItem[] => {
  const lastResult = result[result.length - 1];
  switch (lineAction.itemAction) {
    case ItemAction.Push:
      return [...result].concat(lineAction.item);
    case ItemAction.Add:
      return result.map((r, index) =>
        index === result.length - 1 && lineAction.item?.text !== undefined
          ? { ...r, content: [...(r.content as string[])].concat(lineAction.item?.text) }
          : r
      );
    case ItemAction.AddOrPush:
      if (lastResult && lastResult.type === lineAction.item?.type) {
        lastResult.text += "\n" + lineAction.item?.text;
        return result.map((r, i) =>
          i === result.length - 1 ? { ...r, text: r.text + "\n" + lineAction.item?.text } : r
        );
      } else {
        return [...result].concat(lineAction.item);
      }
    case ItemAction.None:
    default:
      return result;
  }
};

/**
 * Take an hierarchised resultItemsList and hieracies it
 * H3 items will be moved inside of H2 items
 * H2 items will be moved inside of H1 items
 * H1 items will be the root of the resulting list
 *
 * @param resultItems
 * @returns
 */
export const getHierarchy = (resultItems: ResultItem[]): ResultItem[] => {
  const titlesMap = new Map<string, any>([
    [ItemType.H3, {}],
    [ItemType.H2, {}],
    [ItemType.H1, {}],
  ]);
  // get the list of every available titles in the parser (titles will be )
  const titles = Array.from(titlesMap.keys());

  for (const itemType of titles) {
    // get the priority of the item
    const itemTypePriority = itemTypeHierarchies.get(itemType) as number;
    // get all indexes of the actual itemType (itemType will be a title)
    const indexes = resultItems
      .map((v, i) => (v.type === itemType ? i : undefined))
      .filter((v) => v !== undefined) as number[];

    // for every found index
    for (const index of indexes) {
      // create a new empty list for the content of this item
      const indexTypeContent = [];
      // get the item
      const item = resultItems[index];

      // for every next item
      for (let i = index + 1; i < resultItems.length; i++) {
        // get the element
        const element = resultItems[i];

        // get the element's priority
        const elementTypePriority = itemTypeHierarchies.get(element.type) as number;

        // compare it to the item priority
        if (elementTypePriority > itemTypePriority && !titles.includes(element.type.toString())) {
          // if it is superior to the priority of the actual itemType
          // push it to the indexTypeContent list
          indexTypeContent.push(element);
        } else if (titles.includes(element.type.toString()) && elementTypePriority > itemTypePriority) {
          // if the element is a title and the priority is superior to the itemTypePriority (in this case the title priority)
          // get the title item from the map
          const titleItem = titlesMap.get(element.type.toString());
          // get the title content
          const subTitleContent = titleItem[i.toString()];

          if (subTitleContent !== undefined) {
            // if the content is defined, push it to the indexTypeContent list
            indexTypeContent.push(subTitleContent);
          }

          break;
        } else {
          break;
        }
      }

      // push this item to the titleElement with the found subcontent

      // get the title
      const title = titlesMap.get(itemType);

      // set the title content
      title[index.toString()] = { ...item, content: indexTypeContent };
    }
  }

  // get all h1 title values from the titles map
  const h1Values = Object.values(titlesMap.get(ItemType.H1.toString())) as ResultItem[];

  // return it if it has items
  if (h1Values.length > 0) return h1Values;
  // if  not, it means there is no h1 elements in the provided list

  // get all h1 title values from the titles map
  const h2Values = Object.values(titlesMap.get(ItemType.H2.toString())) as ResultItem[];

  // return it if it has items
  if (h2Values.length > 0) return h2Values;
  // if  not, it means there is no h2 elements in the provided list

  // get all h1 title values from the titles map
  const h3Values = Object.values(titlesMap.get(ItemType.H3.toString())) as ResultItem[];

  // return it if it has items
  if (h3Values.length > 0) return h3Values;
  // if  not, it means there is no h2 elements in the provided list

  // if there is no title's elements return the provided list
  return resultItems;
};

/**
 * takes a file path as param, outputs 2 files
 * outputv1.json will be all markdown lines as an array
 * outputv2.json will be all markdown lines hierarchised
 *
 * @param src
 */
const run = (src: string) => {
  const fileLines = getFileLines(src);

  const res = parseMarkdown(fileLines, 0, ParserState.None, []);

  const hierarchy = getHierarchy(res);

  fs.writeFileSync(path.join(__dirname, "./outputv1.json"), JSON.stringify(res));
  fs.writeFileSync(path.join(__dirname, "./outputv2.json"), JSON.stringify(hierarchy));

  console.log("done");
};

const inputFileSrc = "./input.md";

run(inputFileSrc);
