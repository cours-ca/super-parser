import fs from "fs";
import path from "path";

import { expect } from "chai";
import {
  getFileLines,
  processCodeLine,
  processNormalLine,
  ParserState,
  ItemAction,
  ItemType,
  processListLine,
  parseMarkdown,
  getHierarchy,
} from "../src";

const fileName = "test.md";

const fileLinesOld = ["#line1", "##line2", "###line3"];
const fileLines = [
  "  # Titre du document",
  "",
  "Voici un paragraphe",
  "",
  "## Titre de niveau 2",
  "",
  "Un nouveau paragraphe",
  "Toujours le même paragraphe",
  "",
  "Et un nouveau paragraphe !",
  "",
  "```shell",
  "# Un bloc de code en shell",
  'echo "coucou les lapinous"',
  "```",
  "",
  "### Un titre de niv 3",
  "",
  "test",
  "etstg",
  "",
  "- une liste",
  "  # azeqsdfd",
  "  - # sdqd",
  "    - azeaze",
  "      - azeqsd",
  "        - # sdsd",
  "- à",
  "- puce",
  "",
  "```javascript",
  "# Un bloc de code en shell",
  'echo "coucou les lapinous"',
  "```",
];

fs.writeFileSync(path.join(__dirname, fileName), [...fileLines].join("\n"));

describe("Super-parser testing", () => {
  describe("Testing: getFileLines", () => {
    it("both list should be equals", () => {
      const lines = getFileLines(path.join(__dirname, fileName));

      expect(lines).to.eql(fileLines);
    });

    it("should throw an exception", () => {
      expect(() => getFileLines(path.join(__dirname, "unknown"))).to.throw();
    });
  });

  describe("Testing: processCodeLine", () => {
    it("should return action None", () => {
      const action = processCodeLine("```azerty");

      expect(action).to.eql({ newState: ParserState.None, itemAction: ItemAction.None, item: { type: ItemType.None } });
    });

    it("should return action Code", () => {
      const action = processCodeLine("test");

      expect(action).to.eql({
        newState: ParserState.Code,
        itemAction: ItemAction.Add,
        item: { type: ItemType.None, text: "test" },
      });
    });
  });

  describe("Testing: processListLine", () => {
    it("should return action List", () => {
      const action = processListLine("- test");

      expect(action).to.eql({
        newState: ParserState.List,
        itemAction: ItemAction.Add,
        item: { type: ItemType.None, text: " test" },
      });
    });

    it("should return action None", () => {
      const action = processListLine("");

      expect(action).to.eql({ newState: ParserState.None, itemAction: ItemAction.None, item: { type: ItemType.None } });
    });
  });

  describe("Testing: processNormalLine", () => {
    it("should return action Code", () => {
      const action = processNormalLine("```javascript");

      expect(action).to.eql({
        newState: ParserState.Code,
        itemAction: ItemAction.Push,
        item: { type: ItemType.Code, language: "javascript", content: [] },
      });
    });

    it("should return action List", () => {
      const action = processNormalLine("- test");

      expect(action).to.eql({
        newState: ParserState.List,
        itemAction: ItemAction.Push,
        item: { type: ItemType.List, content: [" test"] },
      });
    });

    it("should return action H1", () => {
      const action = processNormalLine("#test");

      expect(action).to.eql({
        newState: ParserState.None,
        itemAction: ItemAction.Push,
        item: { type: ItemType.H1, text: "test" },
      });
    });

    it("should return action H2", () => {
      const action = processNormalLine("##test");

      expect(action).to.eql({
        newState: ParserState.None,
        itemAction: ItemAction.Push,
        item: { type: ItemType.H2, text: "test" },
      });
    });

    it("should return action H3", () => {
      const action = processNormalLine("###test");

      expect(action).to.eql({
        newState: ParserState.None,
        itemAction: ItemAction.Push,
        item: { type: ItemType.H3, text: "test" },
      });
    });

    it("should return action P", () => {
      const action = processNormalLine("test");

      expect(action).to.eql({
        newState: ParserState.None,
        itemAction: ItemAction.AddOrPush,
        item: { type: ItemType.P, text: "test" },
      });
    });

    it("should return action None", () => {
      const action = processNormalLine("");

      expect(action).to.eql({ newState: ParserState.None, itemAction: ItemAction.None, item: { type: ItemType.None } });
    });
  });

  describe("Testing: parseMarkdown", () => {
    it("should return a list of lines", () => {
      const result = parseMarkdown(fileLines, 0, ParserState.None, []);

      expect(result).to.eql([
        { type: "h1", text: " Titre du document" },
        { type: "p", text: "Voici un paragraphe" },
        { type: "h2", text: " Titre de niveau 2" },
        {
          type: "p",
          text: "Un nouveau paragraphe\nToujours le même paragraphe\nToujours le même paragraphe\nEt un nouveau paragraphe !\nEt un nouveau paragraphe !",
        },
        { type: "code", language: "shell", content: ["# Un bloc de code en shell", 'echo "coucou les lapinous"'] },
        { type: "h3", text: " Un titre de niv 3" },
        { type: "p", text: "test\netstg\netstg" },
        { type: "list", content: [" une liste"] },
        { type: "h1", text: " azeqsdfd" },
        { type: "list", content: [" # sdqd", " azeaze", " azeqsd", " # sdsd", " à", " puce"] },
        {
          type: "code",
          language: "javascript",
          content: ["# Un bloc de code en shell", 'echo "coucou les lapinous"'],
        },
      ]);
    });
  });

  describe("Testing: getHierarchy", () => {
    it("should return a list of lines hierarchised with H1", () => {
      const result = getHierarchy([
        { type: ItemType.H1, text: "line1" },
        { type: ItemType.H2, text: "line2" },
        { type: ItemType.H3, text: "line3" },
      ]);

      expect(result).to.eql([
        {
          type: "h1",
          text: "line1",
          content: [{ type: "h2", text: "line2", content: [{ type: "h3", text: "line3", content: [] }] }],
        },
      ]);
    });

    it("should return a list of lines hierarchised with H2", () => {
      const result = getHierarchy([
        { type: ItemType.H2, text: "line2" },
        { type: ItemType.H3, text: "line3" },
      ]);

      expect(result).to.eql([{ type: "h2", text: "line2", content: [{ type: "h3", text: "line3", content: [] }] }]);
    });

    it("should return a list of lines hierarchised with H3", () => {
      const result = getHierarchy([{ type: ItemType.H3, text: "line3" }]);

      expect(result).to.eql([{ type: "h3", text: "line3", content: [] }]);
    });

    it("should return a list of lines hierarchised without titles", () => {
      const result = getHierarchy([{ type: ItemType.P, text: "p1" }]);

      expect(result).to.eql([{ type: "p", text: "p1" }]);
    });
  });
});
